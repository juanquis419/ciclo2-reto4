package f1Teams;

import java.util.Collections;
import java.util.Comparator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Parrilla implements Serializable {

    private ArrayList<Piloto> Pilotos;

    public Parrilla () {

        Persistencia leer = new Persistencia();
        this.Pilotos = leer.abrirArchivo();

        if (this.Pilotos == null) {

            this.Pilotos = new ArrayList<>();

        }

    }

    public ArrayList<Piloto> listar() {

        return this.Pilotos;

    }

    public Piloto buscarPiloto(Integer id) {

        if (id == null) {

            return null;

        } else {

            for (Piloto elPiloto : Pilotos) {

                if (elPiloto.getId() == id){

                    return elPiloto;
                }

            }

        }
        

        return null;

    }

    public boolean addPiloto(Piloto nuevoPiloto) {

        if (this.buscarPiloto(nuevoPiloto.getId()) == null) {

            nuevoPiloto.setId(proxId());

            Pilotos.add(nuevoPiloto);

            Persistencia nuevoAdd = new Persistencia();
            
            if (nuevoAdd.guardar(Pilotos)) {

                return true;
            }


        } else {

            this.editPiloto(nuevoPiloto);
        }

        return false;
    }

    public boolean editPiloto(Piloto elPiloto) {
            
        for (Piloto iPiloto : Pilotos){

            if (Objects.equals(iPiloto.getId(), elPiloto.getId())) {

                iPiloto.setNumero(elPiloto.getNumero());

                iPiloto.setEquipo(elPiloto.getEquipo());

                iPiloto.setNombre(elPiloto.getNombre());

                iPiloto.setSueldo(elPiloto.getSueldo());

                iPiloto.setTitulos(elPiloto.getTitulos());

                iPiloto.setDirector(elPiloto.getDirector());

                iPiloto.setTemporada(elPiloto.getTemporada());
            
                Persistencia editContacto = new Persistencia();

                if (editContacto.guardar(Pilotos)) {

                    return true;
                }

            }

        }

        return false;

    }

    public ArrayList<Piloto> buscarXtemporada(int findTemporada) {
        
        ArrayList<Piloto> pilotosXTemp = new ArrayList<>();

        for (Piloto elPiloto : Pilotos) {

            if (elPiloto.getTemporada() == findTemporada){

                pilotosXTemp.add(elPiloto);

            }

        }
        
        return pilotosXTemp;
    }

    public ArrayList<Piloto> buscarXtemporada(Integer TemporadaInicial, Integer TemporadaFinal) {
        
        ArrayList<Piloto> pilotosXTemp = new ArrayList<>();

        for (Piloto elPiloto : Pilotos) {

            if (elPiloto.getTemporada() >= TemporadaInicial && elPiloto.getTemporada() <= TemporadaFinal){

                pilotosXTemp.add(elPiloto);

            }

        }
        
        return pilotosXTemp;
    }

    public ArrayList<Piloto> buscarXError() {
        
        ArrayList<Piloto> pilotosXError = new ArrayList<>();

        for (Piloto elPiloto : Pilotos) {

            if (elPiloto.getTemporada() == -99999 || elPiloto.getNumero() == -99999 || 
                elPiloto.getSueldo() == -99999 || elPiloto.getTitulos() == -99999){

                pilotosXError.add(elPiloto);

            }

        }
        
        return pilotosXError;
    }

    public boolean borrar(Piloto elPiloto) {
        
       for (Piloto iPiloto : Pilotos) {

            if (Objects.equals(iPiloto.getId(), elPiloto.getId())) {

                this.Pilotos.remove(iPiloto);
                break;

            }
        }

        Persistencia limpieza = new Persistencia();

        if (limpieza.guardar(Pilotos)) {

            return true;
        }

        return false;
    }

    public ArrayList<Piloto> buscarOrdenado() {
        
        Collections.sort(Pilotos, new Comparator<Piloto>() {

            @Override
            public int compare(Piloto piloto1, Piloto piloto2) {
                    return piloto1.getId().compareTo(piloto2.getId());
            }

        });
        
        return this.Pilotos;
    }

    private Integer proxId() {

        if (this.Pilotos.isEmpty()){

            return 1;

        }

        ArrayList<Piloto> lstPilotos = this.buscarOrdenado();
        Integer orden = lstPilotos.get(lstPilotos.size() - 1).getId() + 1;

        return orden;
    }
    
}
