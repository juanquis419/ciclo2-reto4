package f1Teams;

import java.io.Serializable;

public class Piloto implements Serializable {

    private String  director;
    private String  nombre;
    private String  equipo;

    private Integer temporada;
    private Integer titulos;
    private Integer numero;
    private Integer id;

    private Float   sueldo;

    public Piloto(int laTemporada, String elNombre, Float elSueldo, int elNumero, String elEquipo) {
        
        this.temporada = laTemporada;
        this.nombre    = elNombre;
        this.sueldo    = elSueldo;
        this.numero    = elNumero;
        this.equipo    = elEquipo;
    
    }

    // id 
    
    public void setId(Integer elId) {
        this.id = elId;

    }

    public Integer getId() {
        return this.id;

    }


    // titulos

    public void setTitulos(int losTitulos) {
        this.titulos = losTitulos;

    }

    public Integer getTitulos() {
        return this.titulos;

    }

    // numero

    public void setNumero(Integer elNumero) {
        this.numero = elNumero;
    }

    public Integer getNumero() {
        return this.numero;
    }

    // sueldo

    public void setSueldo(Float elSueldo) {
        this.sueldo = elSueldo;
    }

    public Float getSueldo() {
        return this.sueldo;
    }

    // nombre

    public void setNombre(String elNombre) {
        this.nombre = elNombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    // temporada 

    public void setTemporada(Integer laTemporada) {
        this.temporada = laTemporada;
    }

    public Integer getTemporada() {
        return this.temporada;
    }

    // equipo
    
    public void setEquipo(String elEquipo) {
        this.equipo = elEquipo;
    }

    public String getEquipo() {
        return this.equipo;
    }

    // director

    public void setDirector(String elDirector) {
        this.director = elDirector;
    }

    public String getDirector() {
        return this.director;
    }

    public String toString() {
        return "El piloto " + this.nombre + " cuyo número es " + this.numero + " cuenta con " + this.titulos + " y cobra un sueldo de " + this.sueldo + " para la temporada " + this.temporada;
    }

    
}
