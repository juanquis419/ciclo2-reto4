package f1Teams;

import java.io.FileNotFoundException;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Persistencia {
    
    private final String NOMBREARCHIVO    = "pilotos.asi";
    private final String NOMBREARCHIVOLOG = "logRecorder.log";

    public boolean guardar(ArrayList<Piloto> lstPilotos) {

        FileOutputStream fileOpen = null;

        try {

            fileOpen = new FileOutputStream(NOMBREARCHIVO);

            ObjectOutputStream objectOpen = new ObjectOutputStream(fileOpen);

            objectOpen.writeObject(lstPilotos);           

            objectOpen.close();

            return true;

        } catch(FileNotFoundException fnfe){
            
            guardarLog(fnfe.toString());

        } catch (IOException ioe) {

            guardarLog(ioe.toString());

        } finally {
            try {
                
                fileOpen.close();

            } catch (IOException ioe) {

                guardarLog(ioe.toString());
            }
        }

        return false;
    }

    public ArrayList<Piloto> abrirArchivo() {

        ArrayList<Piloto> PilotosArchivo = null;
        FileInputStream fileOpen = null;

        try {

            fileOpen = new FileInputStream(NOMBREARCHIVO);
            
            ObjectInputStream objectOpen = new ObjectInputStream(fileOpen);
            PilotosArchivo = (ArrayList<Piloto>)objectOpen.readObject();
            
            //se cierra archivo

            objectOpen.close();
            fileOpen.close();

            return PilotosArchivo;

        } catch (FileNotFoundException fnfe) {

            guardarLog(fnfe.toString());


        } catch (IOException ioe) {

            guardarLog(ioe.toString());


        } catch (ClassNotFoundException cnfe) {

            guardarLog(cnfe.toString());

        }

        return null;
    }

    public boolean guardarLog(String err) {

        FileOutputStream fileOpen = null;

        try {

            fileOpen = new FileOutputStream(NOMBREARCHIVOLOG);

            ObjectOutputStream objectOpen = new ObjectOutputStream(fileOpen);
            objectOpen.writeObject(err);

            objectOpen.close();

            return true;

        } catch(FileNotFoundException fnfe){
            
            guardarLog(fnfe.toString());

        } catch (IOException ioe) {

            guardarLog(ioe.toString());

        } finally {
            try {
                
                fileOpen.close();

            } catch (IOException ioe) {

                guardarLog(ioe.toString());
            }
        }

        return false;
        
    }

    
}