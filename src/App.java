import f1Teams.*;

import javax.swing.JFrame;
import javax.swing.JTable;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.data.general.DefaultPieDataset;

public class App extends javax.swing.JFrame {

    private Parrilla                parrillaF1   = new Parrilla();
    private Persistencia            persistencia = new Persistencia();
    private Piloto                  pilotoBuscado;

    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;

    private javax.swing.JTable      tPilotos;

    private javax.swing.JTextArea   cjPilotos;
    private javax.swing.JTextField  cjTemporada;
    private javax.swing.JTextField  cjDirector;
    private javax.swing.JTextField  cjTitulos;
    private javax.swing.JTextField  cjNumero;
    private javax.swing.JTextField  cjEquipo;
    private javax.swing.JTextField  cjNombre;
    private javax.swing.JTextField  cjSueldo;
    
    private javax.swing.JButton     btActualizar;
    private javax.swing.JButton     btAdicionar;
    private javax.swing.JButton     btBorrar;
    private javax.swing.JButton     btSalir;
    private javax.swing.JButton     btError;
    private javax.swing.JButton     btGraph;
    private javax.swing.JButton     jButton1;
    private javax.swing.JButton     jButton2;

    private javax.swing.JLabel      jLabel2;
    private javax.swing.JLabel      jLabel3;
    private javax.swing.JLabel      jLabel4;
    private javax.swing.JLabel      jLabel5;
    private javax.swing.JLabel      jLabel6;
    private javax.swing.JLabel      jLabel7;
    private javax.swing.JLabel      jLabel8;
    private javax.swing.JLabel      jLabel9;

    private JFrame                  parent;
    private JFrame                  torta;

    public App() {
        iniciar();
        this.cargarPilotos();
        this.cjPilotos.setVisible(false);
        this.jScrollPane1.setVisible(false);
        
        this.imprimeParilla();
    }

    private void iniciar() {
        
        jScrollPane1    = new javax.swing.JScrollPane();
        jScrollPane2    = new javax.swing.JScrollPane();

        cjTemporada     = new javax.swing.JTextField();
        cjDirector      = new javax.swing.JTextField();
        cjPilotos       = new javax.swing.JTextArea();
        cjTitulos       = new javax.swing.JTextField();
        cjSueldo        = new javax.swing.JTextField();
        cjNombre        = new javax.swing.JTextField();
        cjNumero        = new javax.swing.JTextField();
        cjEquipo        = new javax.swing.JTextField(); 
        
        btActualizar    = new javax.swing.JButton();
        btAdicionar     = new javax.swing.JButton();
        jButton1        = new javax.swing.JButton();
        jButton2        = new javax.swing.JButton();
        btBorrar        = new javax.swing.JButton();
        btSalir         = new javax.swing.JButton();
        btError         = new javax.swing.JButton();
        btGraph         = new javax.swing.JButton();

        jLabel2         = new javax.swing.JLabel();
        jLabel3         = new javax.swing.JLabel();
        jLabel4         = new javax.swing.JLabel();
        jLabel5         = new javax.swing.JLabel();
        jLabel6         = new javax.swing.JLabel();
        jLabel7         = new javax.swing.JLabel();
        jLabel8         = new javax.swing.JLabel();
        jLabel9         = new javax.swing.JLabel();

        tPilotos        = new javax.swing.JTable();

        parent          = new JFrame();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18));
        jLabel8.setText("Parrilla formula 1");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(450, 0, 1500, 70); // check

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel2.setText("Nombre");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 60, 100, 17);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel3.setText("Número");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 90, 100, 17);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel9.setText("Titulos");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(30, 120, 100, 17);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel4.setText("Salario (USD)");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(30, 150, 100, 17);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel5.setText("Equipo");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 180, 100, 17);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel6.setText("Director");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(30, 210, 100, 17);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel7.setText("Temporada");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(30, 240, 100, 17);

        cjNombre.setFont(new java.awt.Font("Tahoma", 0, 14));
        getContentPane().add(cjNombre);
        cjNombre.setBounds(130, 60, 160, 23);

        cjNumero.setFont(new java.awt.Font("Tahoma", 0, 14));
        getContentPane().add(cjNumero);
        cjNumero.setBounds(130, 90, 160, 23);

        cjTitulos.setFont(new java.awt.Font("Tahoma", 0, 14));
        getContentPane().add(cjTitulos);
        cjTitulos.setBounds(130, 120, 160, 23);
        
        cjSueldo.setFont(new java.awt.Font("Tahoma", 0, 14));
        getContentPane().add(cjSueldo);
        cjSueldo.setBounds(130, 150, 160, 23);

        cjEquipo.setFont(new java.awt.Font("Tahoma", 0, 14));
        getContentPane().add(cjEquipo);
        cjEquipo.setBounds(130, 180, 160, 23);

        cjDirector.setFont(new java.awt.Font("Tahoma", 0, 14));
        getContentPane().add(cjDirector);
        cjDirector.setBounds(130, 210, 160, 23);

        cjTemporada.setFont(new java.awt.Font("Tahoma", 0, 14));
        getContentPane().add(cjTemporada);
        cjTemporada.setBounds(130, 240, 160, 23);

        cjPilotos.setColumns(20);
        cjPilotos.setRows(5);
        jScrollPane1.setViewportView(cjPilotos);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(520, 30, 140, 110);

        tPilotos.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseClicked(java.awt.event.MouseEvent evt) {

                tPilotosMouseClicked(evt);

            }

        });

        jScrollPane2.setViewportView(tPilotos);
        tPilotos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(470, 60, 490, 203);

        btAdicionar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btAdicionar.setText("Adicionar");
        btAdicionar.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btAdicionarActionPerformed(evt);

            }
        });
        getContentPane().add(btAdicionar);
        btAdicionar.setBounds(330, 50, 100, 23);

        btActualizar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btActualizar.setText("Actualizar");
        btActualizar.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btActualizarActionPerformed(evt);

            }
        });
        getContentPane().add(btActualizar);
        btActualizar.setBounds(330, 80, 100, 23);

        btBorrar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btBorrar.setText("Borrar");
        btBorrar.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btBorrarActionPerformed(evt);

            }

        });
        getContentPane().add(btBorrar);
        btBorrar.setBounds(330, 110, 100, 23);

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 13));
        jButton1.setText("Temporada");
        jButton1.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                jButton1ActionPerformed(evt);

            }

        });
        getContentPane().add(jButton1);
        jButton1.setBounds(330, 140, 100, 23);

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 14));
        jButton2.setText("Todos");
        jButton2.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                jButton2ActionPerformed(evt);

            }

        });
        getContentPane().add(jButton2);
        jButton2.setBounds(330, 170, 100, 23);

        btError.setFont(new java.awt.Font("Tahoma", 0, 14));
        btError.setText("Errores");

        btError.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                
                btErrorActionPerformed(evt);

            }

        });
        getContentPane().add(btError);
        btError.setBounds(330, 200, 100, 23);

        btGraph.setFont(new java.awt.Font("Tahoma", 0, 14));
        btGraph.setText("Gráfico");

        btGraph.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                
                jbtGraphActionPerformed(evt);

            }

        });
        getContentPane().add(btGraph);
        btGraph.setBounds(330, 230, 100, 23);

        btSalir.setFont(new java.awt.Font("Tahoma", 0, 14));
        btSalir.setText("Salir");

        btSalir.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                
                btSalirActionPerformed(evt);

            }

        });
        getContentPane().add(btSalir);
        btSalir.setBounds(330, 260, 100, 23);

        setSize(new java.awt.Dimension(1000, 350));
        setLocationRelativeTo(null);
        setResizable(false);
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {

        this.cargarPilotos();

    }

    private void jbtGraphActionPerformed(java.awt.event.ActionEvent evt) {

        this.graficoSalarios();

    }


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

        this.temporada();

    }

    private void btSalirActionPerformed(java.awt.event.ActionEvent evt) {
        
        this.salir();
    
    }
    private void btErrorActionPerformed(java.awt.event.ActionEvent evt) {
        
        this.errores();
    
    }

    private void btActualizarActionPerformed(java.awt.event.ActionEvent evt) {

        this.editarPiloto();

    }

    private void tPilotosMouseClicked(java.awt.event.MouseEvent evt) {

        this.seleccionoFila();
    }

    private void btBorrarActionPerformed(java.awt.event.ActionEvent evt) {

        this.borrar();
    }

    private void btAdicionarActionPerformed(java.awt.event.ActionEvent evt) {

        this.adicionarPiloto();
    }

    private void salir(){

        System.exit(0);

    }
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {

                new App().setVisible(true);

            }

        });

    }

    private void adicionarPiloto() {

        Float   sueldo;
        Integer numero;
        Integer titulos;
        Integer temporada;

        String nombre       = cjNombre.getText().replaceAll("\\s+"," ").toUpperCase().trim();

        String numeroStr    = cjNumero.getText().replaceAll("\\s+"," ").trim();
        try {

            numero      = Integer.parseInt(numeroStr);

        } catch (Exception e) {

            numero = -99999;
            persistencia.guardarLog(e.toString());

        }

        String director     = cjDirector.getText().replaceAll("\\s+"," ").toUpperCase().trim();

        String equipo       = cjEquipo.getText().replaceAll("\\s+"," ").toUpperCase().trim();

        String temporadaStr = cjTemporada.getText().replaceAll("\\s+"," ").trim();
        try {

            temporada   = Integer.parseInt(temporadaStr);

        } catch (Exception e) {

            temporada = -99999;
            persistencia.guardarLog(e.toString());

        }

        String titulosStr   = cjTitulos.getText().replaceAll("\\s+"," ").trim();
        try {

            titulos     = Integer.parseInt(titulosStr);

        } catch (Exception e) {

            titulos = -99999;
            persistencia.guardarLog(e.toString());

        }

        String sueldoStr    = cjSueldo.getText().replaceAll("\\s+"," ").trim();
        try {
                
            sueldo        = Float.parseFloat(sueldoStr);

        } catch (Exception e) {

            sueldo = -99999F;
            persistencia.guardarLog(e.toString());

        }

        Piloto nuevoPiloto = new Piloto(temporada, nombre, sueldo, numero, equipo);
        nuevoPiloto.setDirector(director);
        nuevoPiloto.setTitulos(titulos);
        
        if (parrillaF1.addPiloto(nuevoPiloto)){

            JOptionPane.showMessageDialog(this, "Se creo a " + nuevoPiloto.getNombre());

        } else {

            JOptionPane.showMessageDialog(this, "No se puedo crear el piloto");

        }
        
        this.imprimeParilla();
        cargarPilotos();
        
    }

    private void editarPiloto() {

        Float   sueldo;
        Integer numero;
        Integer titulos;
        Integer temporada;

        if (pilotoBuscado != null) {

            String nombre       = cjNombre.getText().replaceAll("\\s+"," ").toUpperCase().trim();

            String numeroStr    = cjNumero.getText().replaceAll("\\s+"," ").trim();
            try {

                numero      = Integer.parseInt(numeroStr);

            } catch (Exception e) {

                numero = -99999;
                persistencia.guardarLog(e.toString());

            }
            
            String director     = cjDirector.getText().replaceAll("\\s+","").toUpperCase().trim();
            String equipo       = cjEquipo.getText().replaceAll("\\s+"," ").toUpperCase().trim();

            String temporadaStr = cjTemporada.getText().replaceAll("\\s+"," ").trim();
            try {

                temporada   = Integer.parseInt(temporadaStr);

            } catch (Exception e) {

                temporada = -99999;
                persistencia.guardarLog(e.toString());

            }
            

            String titulosStr   = cjTitulos.getText().replaceAll("\\s+"," ").trim();
            try {

                titulos     = Integer.parseInt(titulosStr);

            } catch (Exception e) {

                titulos = -99999;
                persistencia.guardarLog(e.toString());

            }
            

            String sueldoStr    = cjSueldo.getText().replaceAll("\\s+"," ").trim();
            try {
                
                sueldo        = Float.parseFloat(sueldoStr);

            } catch (Exception e) {

                sueldo = -99999F;
                persistencia.guardarLog(e.toString());

            }
            
            pilotoBuscado.setTemporada(temporada);
            pilotoBuscado.setDirector(director);
            pilotoBuscado.setTitulos(titulos);
            pilotoBuscado.setNombre(nombre);
            pilotoBuscado.setNumero(numero);
            pilotoBuscado.setSueldo(sueldo);
            pilotoBuscado.setEquipo(equipo);

            if (parrillaF1.editPiloto(pilotoBuscado)){

                JOptionPane.showMessageDialog(this, "Se actualizó a " + pilotoBuscado.getNombre());

            } else {

                JOptionPane.showMessageDialog(this, "No se puedo actualizó el piloto");

            }
            
            this.imprimeParilla();
            cargarPilotos();
        }
        
    }

    private void imprimeParilla(){

        this.cjPilotos.append("ID \t Nombre \t Número \t Titulos \t Salario (USD) \t Equipo \t Director \t Temporada\n");
        
        for (Piloto iPiloto : this.parrillaF1.listar()) {
            
              this.cjPilotos.append(iPiloto.getId().toString()      + "\t" +
                                    iPiloto.getNombre()             + "\t" +
                                    iPiloto.getNumero().toString()  + "\t" +
                                    iPiloto.getTitulos().toString() + "\t" +
                                    iPiloto.getSueldo().toString()  + "\t "+
                                    iPiloto.getEquipo()             + "\t "+  
                                    iPiloto.getDirector()           + "\t "+
                                    iPiloto.getTemporada()          + "\n "       
                                    );

        }
    }
    
    private void cargarPilotos() {
        
        String columnas[] = {"ID", "Nombre", "Número", "Titulos" ,"Salario (USD)", "Equipo", "Director", "Temporada"};
        
        DefaultTableModel modelo = new DefaultTableModel();

        for (int i = 0; i < columnas.length; i++){
            
            modelo.addColumn(columnas[i]);
        
        }

        for (Piloto piloto : this.parrillaF1.buscarOrdenado()) {
            
            Object[] datos = {

                piloto.getId(),

                piloto.getNombre(),

                piloto.getNumero(),

                piloto.getTitulos(),

                piloto.getSueldo(),

                piloto.getEquipo(),

                piloto.getDirector(),

                piloto.getTemporada(),
            };
            
            modelo.addRow(datos);
                    
        }
        
        this.tPilotos.setModel(modelo);

    }

    private void temporada() {

        Integer temp1;
        Integer temp2;

        String columnas[] = {"ID", "Nombre", "Número", "Titulos" ,"Salario (USD)", "Equipo", "Director", "Temporada"};
        
        DefaultTableModel modelo = new DefaultTableModel();
        
        JOptionPane optionPane = new JOptionPane("¿Desea ingresar una temporada?, si desea un rango ingrese no", JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
        JDialog dialog = optionPane.createDialog(parent, "Selector de temporada");
        dialog.setVisible(true);

        for (int i = 0; i < columnas.length; i++){
            
            modelo.addColumn(columnas[i]);
        
        } 
        
        String  temp1Str = JOptionPane.showInputDialog("Input temporada inicial: ");

        try {

            temp1 = Integer.parseInt(temp1Str); 

        } catch (Exception e){

            temp1 = null;
            persistencia.guardarLog(e.toString());
        }
        
        if (temp1 != null) {

            if (optionPane.getValue().toString().equals("0")) {
    
                for (Piloto piloto : this.parrillaF1.buscarXtemporada(temp1)) {
                
                    Object[] datos = {
        
                        piloto.getId(),

                        piloto.getNombre(),

                        piloto.getNumero(),

                        piloto.getTitulos(),

                        piloto.getSueldo(),

                        piloto.getEquipo(),

                        piloto.getDirector(),

                        piloto.getTemporada(),
                    };
                    
                    modelo.addRow(datos);
                            
                }
    
            } else if (optionPane.getValue().toString().equals("1")) {
    
                String  temp2Str = JOptionPane.showInputDialog("Input temporada final: ");
                
                try {
            
                    temp2 = Integer.parseInt(temp2Str);
        
                } catch (Exception e) {
        
                    temp2 = null;
                    persistencia.guardarLog(e.toString());
                }

                if (temp2 != null){

                    for (Piloto piloto : this.parrillaF1.buscarXtemporada(temp1, temp2)) {
                
                        Object[] datos = {
            
                            piloto.getId(),

                            piloto.getNombre(),

                            piloto.getNumero(),

                            piloto.getTitulos(),

                            piloto.getSueldo(),

                            piloto.getEquipo(),

                            piloto.getDirector(),

                            piloto.getTemporada(),
                        };
                        
                        modelo.addRow(datos);
                                
                    }

                } else {
                    
                    JOptionPane.showMessageDialog(this, "No se realizó el filtro, debido a que ingreso el segundo valor mal");
                
                }
    
            }
            
        } else {

            JOptionPane.showMessageDialog(this, "No se realizó el filtro");
        }
             
        this.tPilotos.setModel(modelo);

    }

    private void errores() {
        
        DefaultTableModel modelo = new DefaultTableModel();

        String columnas[] = {"ID", "Nombre", "Número", "Titulos" , "Salario (USD)", "Equipo", "Director", "Temporada"};

        for (int i = 0; i < columnas.length; i++){
            
            modelo.addColumn(columnas[i]);
        
        }

        for (Piloto piloto : this.parrillaF1.buscarXError()) {
                
            Object[] datos = {

                piloto.getId(),

                piloto.getNombre(),

                piloto.getNumero(),

                piloto.getTitulos(),

                piloto.getSueldo(),

                piloto.getEquipo(),

                piloto.getDirector(),

                piloto.getTemporada(),

            };
            
            modelo.addRow(datos);
                    
        }

        this.tPilotos.setModel(modelo);

    }

    private void seleccionoFila() {

        int     fila    = tPilotos.getSelectedRow();
        Integer id      = (Integer)tPilotos.getValueAt(fila, 0);

        pilotoBuscado = this.parrillaF1.buscarPiloto(id);

        if (pilotoBuscado != null){

            this.cjTemporada.setText(pilotoBuscado.getTemporada().toString());
            this.cjTitulos.setText(pilotoBuscado.getTitulos().toString());
            this.cjNumero.setText(pilotoBuscado.getNumero().toString());
            this.cjSueldo.setText(pilotoBuscado.getSueldo().toString());

            this.cjDirector.setText(pilotoBuscado.getDirector());
            this.cjNombre.setText(pilotoBuscado.getNombre());
            this.cjEquipo.setText(pilotoBuscado.getEquipo());

        }
                
    }

    private void graficoSalarios() {
        
        torta = new JFrame();

        DefaultPieDataset dataset = new DefaultPieDataset();
        
        for (Piloto iPiloto : parrillaF1.buscarOrdenado()){

            dataset.setValue(iPiloto.getNombre(), iPiloto.getSueldo());

        } 
        
        JFreeChart chart = ChartFactory.createPieChart(// char t
                
                "Torta de salario de los pilotos ",// title                                                                     
                dataset, // data
                true, // include legend
                true, false);
        
        
        ChartPanel panel = new ChartPanel(chart);
        
        //Se crean ventana

        torta.setVisible(true);
        torta.setSize(800, 600);
        
        
        torta.add(panel);

    }

    private void borrar(){

        if (pilotoBuscado!= null){

            if (this.parrillaF1.borrar(pilotoBuscado)){

                JOptionPane.showMessageDialog(this, "Se borró a " + pilotoBuscado.getNombre());

            } else {

                JOptionPane.showMessageDialog(this, "No se pudo borrar el piloto " + pilotoBuscado.getNombre());
                
            }

            cargarPilotos();
            
        }
        
    }

}